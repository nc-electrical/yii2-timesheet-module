<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Invitation */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Invitations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="invitation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('nc', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('nc', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('nc', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'meeting_id',
            'user_id',
            'approved_at',
            'accepted',
        ],
    ]) ?>

</div>
