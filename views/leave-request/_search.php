<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\LeaveRequestSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="leave-request-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'approved_by') ?>

    <?= $form->field($model, 'state') ?>

    <?= $form->field($model, 'code') ?>

    <?php // echo $form->field($model, 'note') ?>

    <?php // echo $form->field($model, 'approval_note') ?>

    <?php // echo $form->field($model, 'all_day') ?>

    <?php // echo $form->field($model, 'start_date') ?>

    <?php // echo $form->field($model, 'end_date') ?>

    <?php // echo $form->field($model, 'number') ?>

    <?php // echo $form->field($model, 'approved_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('nc', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('nc', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
