<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\File */

$this->title = $model->filename;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="file-view">

    <p class="pull-right">
        <?= Html::a(Yii::t('nc', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('nc', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('nc', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('<span class="glyphicon glyphicon-download"></span> Download', $model->url, [
              'title' => Yii::t('nc', 'Download'),
              'target' => '_blank',
              'class' => 'linksWithTarget btn btn-default',
              'data-pjax' => '0',
          ]); ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>

    <blockquote class="text text-info"><?= $model->description; ?></blockquote>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'filename',
            'type',
            'description:ntext',
            'thumbs:ntext',
            'created_at:datetime',
            'createdBy.username:ntext:'.Yii::t('nc', 'Created By'),
            'updated_at:datetime',
            'updatedBy.username:ntext:'.Yii::t('nc', 'Updated By'),
        ],
    ]) ?>

</div>
