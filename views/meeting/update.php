<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Meeting */

$this->title = Yii::t('nc', 'Update {modelClass}: ', [
    'modelClass' => 'Meeting',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Meetings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('nc', 'Update');
?>
<div class="meeting-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
