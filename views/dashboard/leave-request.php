<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\LeaveRequest */

$this->title = Yii::t('nc', 'Create Leave Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Leave Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="leave-request-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row"><div class="col-md-6">
          <?= $form->field($model, 'start_date')->widget(DatePicker::className()) ?>
        </div><div class="col-md-6">

          <?= $form->field($model, 'end_date')->widget(DatePicker::className()) ?>
        </div></div>

        <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('nc', 'Submit'), ['class' => 'btn btn-warning']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
