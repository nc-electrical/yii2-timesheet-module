<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use nc\timesheet\models\TimeEntry;

// Notification message 
if ($model->state == 1)
Yii::$app->session->addFlash('info', 'Your timesheet is locked for further processing');
elseif ($model->state == 2)
Yii::$app->session->addFlash('success', 'Your timesheet is already approved');

$events = ArrayHelper::index($model->entries, 'id', 'date');
$summary = []; $total = 0;
foreach ($model->entries as $e) {
  if (!array_key_exists($key = $e->jobCode->name, $summary)) $summary[$key] = 0;
  $summary[$key] += $e->duration; $total += $e->duration;
}
/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Timesheet */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Timesheet'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="btn-group pull-right" role="group">
    <?= Html::a(Yii::t('nc', '&larr; Previous'), ['', 'year' => date('Y', $lastweek = strtotime($model->dateFrom . ' -1 week')), 'week' => date('W', $lastweek)], ['class' => 'btn btn-default']) ?>
    <?= Html::a(Yii::t('nc', 'This Week'), ['', 'year' => date('Y'), 'week' => date('W')], ['class' => 'btn btn-default']) ?>
    <?= Html::a(Yii::t('nc', 'Next &rarr;'), ['', 'year' => date('Y', $nextweek = strtotime($model->dateFrom . ' +1 week')), 'week' => date('W', $nextweek)], ['class' => 'btn btn-default']) ?>
</div>

<?php if ($model->state > 0): ?>
  <div class="pull-right">
    <?php $form = ActiveForm::begin(['action' => ['export-pdf'], 'enableClientValidation' => false, 'id' => 'pdf-form']); ?>
    <textarea id="html-content" name="content" style="display:none;"></textarea><?php ActiveForm::end(); ?>
    <?= Html::tag('span', Html::icon('save') . ' Export PDF', ['id' => 'export-pdf', 'class' => 'btn btn-primary']); ?>
<?php
$exportPdfUrl = Url::to(['export-pdf']);
$this->registerJs("$(document).ready(function(){
  $('#export-pdf').click(function(e){
    e.preventDefault();
    var content = $('.print-me').html();
    $('#html-content').val(content);
    $('#pdf-form').submit();
  });
});", \yii\web\View::POS_READY); ?>
  </div>
<?php endif; ?>

<div class="print-me">


<h1><small><?= Yii::t('nc', 'Timesheet') ?></small> <?= Html::encode($this->title) ?> <span class="badge"><?= $model::state($model->state); ?></span></h1>

<div class="timesheet-view">
  <div class="table-responsive">
  <table class="table table-bordered">
    <thead>
      <tr>
      <?php $date = $model->dateFrom;
      $end_date = $model->dateTo;
      $dates = [];
      while ($date <= $end_date){ ?>
        <th><?= date('D d/m', strtotime($date)); ?></th>
      <?php
        $dates[] = $date;
        $date = date('Y-m-d', strtotime("$date + 1 day"));
      }  ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php foreach ($dates as $date): ?>
          <td class="event-panel"><div style="min-height: 200px;">
            <?php foreach (ArrayHelper::getValue($events, $date, []) as $id => $e): ?>
              <?= Html::tag('h5', Yii::t('nc', '{icon} {duration} hr: {jobcode}', [
                'icon' => Html::icon('time'),
                'duration' => $e['duration'],
                'jobcode' => $e->jobCode->name,
                ]),[
                  'style' => ['color' => $e->jobCode->color],
                'title' => $e['description'],
                'class' => 'btn btn-default btn-block event-btn',
                'data-date' => $date,
                'data-id' => $id,
                'data-duration' => $e['duration'],
                'data-description' => $e['description'],
                'data-jobcode_id' => $e['jobcode_id'],
              ]); ?>
            <?php endforeach; ?>
          </div></td>
        <?php endforeach; ?>
      </tr>
    </tbody>
    <?php if ($model->state <= 0): ?>
    <tfoot>
      <tr>
        <?php foreach ($dates as $date): ?>
          <td>
            <?= Html::tag('span', Html::icon('pencil'), [
              'class' => 'btn btn-default btn-block event-new',
              'data-date' => $date,
              'title' => Yii::t('nc', 'Enter new Entry'),
            ]); ?>
          </td>
        <?php endforeach; ?>
      </tr>
    </tfoot>
    <?php endif; ?>
  </table>

</div>
</div>
  <h3><?= Yii::t('nc', 'Time Entries'); ?></h3>
  <?= GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $model->entries]),
    'columns' => [
      'date',
      ['attribute' => 'date', 'value' => function($d){ return date('l', strtotime($d->date)); }, 'format' => 'ntext', 'label' => 'Date of Week'],
      'jobCode.name:ntext:Job Code',
      'description:ntext',
      'duration:integer',
    ]

  ]); ?>

<div class="row">
  <div class="col-md-6">
    <div class="panel panel-info">
      <div class="panel-heading"><?= Yii::t('nc', 'Extra Information') ?></div>
      <div class="panel-body">
    <?php if ($model->state <= 0): ?>
      <?php if ($model->approval_note): ?>
        <p class="text-warning"><?= Yii::t('nc', 'The timesheet is rejected by {mgr} at {date} with following reason:', ['mgr' => ArrayHelper::getValue($model->updatedBy, 'username'), 'date' => date('Y-m-d', $model->updated_at)]); ?></p>
        <blockquote><?= $model->approval_note; ?></blockquote>
      <?php endif; ?>
      <?php $form = ActiveForm::begin(); ?>

      <?= $form->field($model, 'comment')->textarea(['rows' => 4]) ?>

      <?= Html::submitButton(Yii::t('nc', '{icon} Submit For Approval', ['icon' => Html::icon('send')]), ['class' => 'btn btn-warning pull-left', 'name' => $model->formName() . "[state]", 'value' => 1]) ?>

      <?= Html::submitButton(Yii::t('nc', '{icon} Save Draft', ['icon' => Html::icon('save')]), ['class' => 'btn btn-primary pull-right', 'name' => $model->formName() . "[state]", 'value' => 0]) ?>

      <?php ActiveForm::end(); ?>

      <?php Modal::begin([
        'id' => 'time-entry-modal',
        'header' => '<h2 id="time-entry-title">Add / Update Time Entry</h2>',
      ]);

      echo $this->render('_time-entry', ['model' => new TimeEntry()]);

      Modal::end();
      $this->registerJs("
      $(document).ready(function(){
        $('.event-new').click(function(e){
          e.preventDefault();
          $('#timeentry-id').val('');
          $('#timeentry-description').val('');
          $('#timeentry-duration').val('');
          $('#timeentry-date').val(this.dataset.date);
          $('#time-entry-modal').modal();
        });
        $('.event-btn').click(function(e){
          e.preventDefault();
          for (var key in this.dataset){
            $('#timeentry-' + key).val(this.dataset[key]);
          }
          $('#time-entry-modal').modal();
        });
      });", \yii\web\View::POS_READY);
      ?>

    <?php else: ?>
      <blockquote><p><?= $model->comment; ?></p></blockquote>

      <?= DetailView::widget([
          'model' => $model,
          'attributes' => [
              'approvedBy.username:ntext:Approved',
              'approved_at',
              [ 'label' => 'state',
                'value' => ArrayHelper::getValue(Yii::$app->params, "timesheet-status.{$model['state']}")
              ],
              'comment:ntext',
              'status',
          ],
      ]) ?>
    <?php endif; ?>
  </div></div>
</div><div class="col-md-6">
  <h4 class="text-info"><?= Yii::t('nc', 'Total time spent by Job Code'); ?></h4>
  <ul class="summary">
  <?php foreach($summary as $label => $value): ?>
    <li><strong><?= $label ?>:</strong> <span class="pull-right"><?= $value ?> hr</span></li>
  <?php endforeach; ?>
  </ul>
  <hr>
  <ul class=""><li>
  <strong><?= Yii::t('nc', 'Total') ?>:</strong> <span class="pull-right"><?= $total ?> hr</span></li></ul>
</div></div>
</div>
</div>
