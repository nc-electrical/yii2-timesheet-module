<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = "Main Menu";
?>

<div class="panel panel-info">
  <div class="panel-heading">
    <?= Html::a(Yii::t('nc', '{icon} Create Meeting', ['icon' => Html::icon('time')]), ['meeting'], ['class' => 'btn btn-primary pull-right']) ?>
    <h4><?= Yii::t('nc', '{icon} My Meeting', ['icon' => Html::icon('briefcase')]) ?></h4>
  </div>
  <div class="panel-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'name',
                'note:ntext',
                // 'owner.username:ntext:Owner',
                'start_date',
                'end_date',
                'all_day:boolean',
                [ 'class' => 'yii\grid\ActionColumn',
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['meeting', 'id' => $model->id], [
                          'title' => Yii::t('nc', 'View'),
                      ]);
                    }
                  ],
                ],
              ],
        ]); ?>
    <?php Pjax::end(); ?>
  </div>
</div>
