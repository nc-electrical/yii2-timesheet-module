<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = "Main Menu";
?>

<div class="panel panel-info">
  <div class="panel-heading">
    <?= Html::a(Yii::t('nc', '{icon} Quick Upload File', ['icon' => Html::icon('file')]), ['batch-upload'], ['class' => 'btn btn-info pull-right']) ?>
    <h4><?= Yii::t('nc', '{icon} My Files', ['icon' => Html::icon('file')]) ?></h4>
  </div>
  <div class="panel-body">
    <?= Html::a(Yii::t('nc', '{icon} New File', ['icon' => Html::icon('file')]), ['upload-file'], ['class' => 'btn btn-default pull-right']) ?>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'filename',
                'description',
                [ 'class' => 'yii\grid\ActionColumn',
                  'template' => '{download}',
                  'buttons' => [
                    'download' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-download"></span>', $model->url, [
                          'title' => Yii::t('nc', 'Download'),
                          'target' => '_blank',
                          'class' => 'linksWithTarget',
                          'data-pjax' => '0',
                      ]);
                    }
                  ]],
              ],
        ]); ?>
    <?php Pjax::end(); ?>
  </div>
</div>
