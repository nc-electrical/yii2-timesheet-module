<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\LeaveRequest */

$this->title = Yii::t('nc', 'Create Files');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="files-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="leave-request-form">

        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?= $form->field($model, 'uploadFile')->widget(FileInput::className()) ?>

        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('nc', 'Submit'), ['class' => 'btn btn-warning']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
