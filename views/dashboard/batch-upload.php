<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\File */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('nc', 'Batch Upload File');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Files'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="file-batch">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'batchUpload[]')->widget(FileInput::className(), ['options' => ['multiple' => true]]) ?>

    <?= $form->field($model, 'description')->textArea()->hint('This text will be concatenate with the uploaded file name to create unique description.') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('nc', 'Upload'), ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
