<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use dektrium\user\models\User;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\meetingRequest */
// TODO: Check if current user is one of the owner of the meeting...
// If he is, then we can skip the Accept / Deline part ...
// If not, display the form....

$this->title = Yii::t('nc', 'Create Meeting Request');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'meeting Requests'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$userText =  ($model->userIDs)?ArrayHelper::getColumn(User::find()->where(['in', 'id', $model->userIDs])->all(), 'username'):[];
?>
<div class="meeting-request-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="meeting-request-form">

        <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name') ?>

        <?= $form->field($model, 'userIDs')->widget(Select2::className(), [
                'options' => [
                        'multiple' => true,
                        'placeholder' => '-- Invite Users --'
                ],
                'initValueText' => $userText,
                'pluginOptions' => [
                  'minimumInputLength' => 3,
                  'ajax' => [
                      'url' => Url::to('lookup'),
                      'dataType' => 'json',
                      'data' => new JsExpression("function(params) { return {q:params.term}; }")
                  ],
                ]]); ?>


        <div class="row"><div class="col-md-6">
          <?= $form->field($model, 'start_date')->widget(DateTimePicker::className()) ?>
        </div><div class="col-md-6">

          <?= $form->field($model, 'end_date')->widget(DateTimePicker::className()) ?>
        </div></div>

        <?= $form->field($model, 'note')->textarea(['rows' => 6]) ?>

        <?= $form->field($model, 'all_day')->checkbox() ?>


        <div class="form-group">
            <?= Html::submitButton(Yii::t('nc', 'Submit'), ['class' => 'btn btn-warning']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

</div>
