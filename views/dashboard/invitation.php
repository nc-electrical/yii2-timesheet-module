<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use kartik\widgets\DateTimePicker;
use kartik\widgets\Select2;
use dektrium\user\models\User;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\meetingRequest */
// TODO: Check if current user is one of the owner of the meeting...
// If he is, then we can skip the Accept / Deline part ...
// If not, display the form....

$this->title = Yii::t('nc', 'Meeting Invitation');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Dashboard'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="col-md-6">
  <div class="panel panel-info">
    <div class="panel-heading"><small><?= Yii::t('nc', 'Meeting'); ?>:</small> <?= $model->name; ?></div>
    <div class="panel-body">
      <p class="text-info"><?= Yii::t('nc', 'Sender: {owner}', ['owner' => $model->owner->username]); ?></p>
      <p class="text-info"><?= Yii::t('nc', 'Time: {start} - {end}', ['start' => $model->start_date, 'end' => $model->end_date]); ?></p>
      <blockquote><?= $model->note; ?></blockquote>
    </div>
  </div>
</div>
<div class="col-md-6">
  <div class="panel panel-primary">
    <div class="panel-heading"><?= Yii::t('nc', 'Your Decission?'); ?></div>
    <div class="panel-body">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($invitation, 'note')->textArea(['cols' => 6]) ; ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('nc', 'Accept'), ['class' => 'btn btn-success pull-left', 'name' => $invitation->formName(). '[accepted]', 'value' => 1]) ; ?>
        <?= Html::submitButton(Yii::t('nc', 'Decline'), ['class' => 'btn btn-warning pull-right', 'name' => $invitation->formName(). '[accepted]', 'value' => -1]) ; ?>
    </div>

    <?php ActiveForm::end(); ?>
  </div>
  </div>
</div>
