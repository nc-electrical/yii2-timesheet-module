<?php
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
$this->title = "Main Menu";
?>

<div class="panel panel-success">
  <div class="panel-heading">
    <?= Html::a(Yii::t('nc', '{icon} Create Timesheet', ['icon' => Html::icon('calendar')]), ['timesheet'], ['class' => 'btn btn-success pull-right']) ?>
    <h4><?= Yii::t('nc', '{icon} My Timesheet', ['icon' => Html::icon('calendar')]) ?></h4>
  </div>
  <div class="panel-body">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'title',
                // 'owner.username:ntext:Owner',
                'approvedBy.username:ntext:Approved By',
                'approved_at',
                'comment:ntext',
                [ 'attribute' => 'state',
                  'value' => function($model){
                    return $model::state($model['state']);
                  }
                ],
                [ 'class' => 'yii\grid\ActionColumn',
                  'template' => '{view}',
                  'buttons' => [
                    'view' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['timesheet', 'year' => $model->year, 'week' => $model->week], [
                          'title' => Yii::t('nc', 'View'),
                      ]);
                    }
                  ]],
              ],
        ]); ?>
    <?php Pjax::end(); ?>

  </div>
</div>
