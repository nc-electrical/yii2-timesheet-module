<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\RangeInput;
use nc\timesheet\models\JobCode;



/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\TimeEntry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="time-entry-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php //= $form->field($model, 'timesheet_id')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jobcode_id')->dropDownList(ArrayHelper::map(JobCode::find()->all(), 'id', 'name')) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'duration')->widget(RangeInput::classname(), [
        'options' => ['placeholder' => Yii::t('nc', 'Enter work hours...')],
        'html5Options' => ['min'=> 0, 'max'=>24, 'step'=>0.5],
        'addon' => ['append'=>['content'=>'<i class="glyphicon glyphicon-time"></i>']]
    ]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('nc', 'Create') : Yii::t('nc', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
