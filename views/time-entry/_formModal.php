<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\RangeInput;
use nc\timesheet\models\JobCode;



/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\TimeEntry */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="time-entry-form">

    <?php $form = ActiveForm::begin([
      'action' => ['save'],
      // 'enableClient  Validation' => false,
      'enableAjaxValidation' => false,
    ]); ?>

    <?= $form->field($model, 'id')->hiddenInput()->label(false) ?>

    <?= $form->field($model, 'jobcode_id')->dropDownList(ArrayHelper::map(JobCode::find()->all(), 'id', 'name')) ?>

    <div class="row"><div class="col-md-8">
    <?= $form->field($model, 'date')->widget(DatePicker::className()) ?>
    </div><div class="col-md-4">
    <?= $form->field($model, 'duration')->textInput(['type' => 'number'])?>
  </div></div>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('nc', 'Save') : Yii::t('nc', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
