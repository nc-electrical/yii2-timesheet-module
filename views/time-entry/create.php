<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\TimeEntry */

$this->title = Yii::t('nc', 'Create Time Entry');
$this->params['breadcrumbs'][] = ['label' => Yii::t('nc', 'Time Entries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-entry-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
