<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\widgets\DatePicker;
use nc\timesheet\models\TimesheetStatus;
/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Timesheet */
/* @var $form yii\widgets\ActiveForm */
// FIXME: Add a datepicker and auto-calculate the weekOfYear, month, year field
?>

<div class="timesheet-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'approved_by')->textInput() ?>

    <?= $form->field($model, 'approved_at')->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'date')->widget(DatePicker::className()) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'state')->dropDownList(Yii::$app->params['timesheet-status']) ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('nc', 'Create') : Yii::t('nc', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
