<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
/* @var $this yii\web\View */
/* @var $searchModel nc\timesheet\models\TimesheetSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('nc', 'Timesheets');
$this->params['breadcrumbs'][] = $this->title;
// Support change approval state in batch mode
$this->registerJs("
    $(document).ready(function(){
      $('.batchAction').click(function(e){
        e.preventDefault();
        var keys = $('#timesheet-list').yiiGridView('getSelectedRows');
        var csrf = yii.getCsrfToken();
        var btnUrl = $(this).attr('href');
        $.ajax({
            type: 'POST',
            url : btnUrl,
            data : {row_id: keys, _csrf: csrf},
            success : function() {
              location.reload();
            },
            error : function(){
              location.reload();
            }
        });
    });
});", \yii\web\View::POS_READY);
?>
<div class="timesheet-index">

    <p class="pull-right">With selected:
      <?= Html::a(Yii::t('nc', 'Approve'), ['batch-approval', 'state' => 2], ['class' => 'btn btn-success batchAction']) ?>
      <?= Html::a(Yii::t('nc', 'Reject'), ['batch-approval', 'state' => -1], ['class' => 'btn btn-danger batchAction']) ?>
      <?= Html::a(Yii::t('nc', 'Need Correction'), ['batch-approval', 'state' => -2], ['class' => 'btn btn-warning batchAction']) ?>
    </p>

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-default"><div class="panel-body">
      <?php $form = ActiveForm::begin(['method' => 'get'])?>
          <div class="row">
                  <div class="col-sm-3">
                      <?= $form->field($searchModel, 'dateStart')->widget(DatePicker::className())->label(false)?>
                  </div><div class="col-sm-3">
                      <?= $form->field($searchModel, 'dateEnd')->widget(DatePicker::className())->label(false) ?>
                    </div><div class="col-sm-4">
                      <?= $form->field($searchModel, 'userIDs')->widget(Select2::className(), [
                         'options' => [
                           'multiple' => true,
                           'placeholder' => $searchModel->getAttributeLabel('userIDs')
                         ],
                         'data' => $searchModel->ownerUsernames,
                         'pluginOptions' => [
                           'allowClear' => true,
                           'minimumInputLength' => 3,
                           'ajax' => [
                               'url' => Url::to(['/timesheet/user/lookup']),
                               'dataType' => 'json',
                               'data' => new JsExpression("function(params) { return {q:params.term}; }")
                           ],
                         ]])->label(false) ?>
                  </div><?php /*div class="col-sm-6">
                      <?= $form->field($searchModel, 'approvedByIDs')->widget(Select2::className(), [
                         'options' => [
                           'multiple' => true,
                         ],
                         'data' => $searchModel->approvedByUsernames,
                         'pluginOptions' => [
                           'allowClear' => true,
                           'minimumInputLength' => 3,
                           'ajax' => [
                               'url' => Url::to(['/timesheet/user/lookup']),
                               'dataType' => 'json',
                               'data' => new JsExpression("function(params) { return {q:params.term}; }")
                           ],
                         ]]) ?>
                  </div*/?><div class="col-sm-2">
                          <?= Html::submitButton(Yii::t('nc', 'Search'), ['class' => 'btn btn-primary pull-right']) ?>
                  </div>
          </div>
        <?php ActiveForm::end()?>
      </div></div>


    <?php Pjax::begin(); ?>    <?= GridView::widget([
            'id' => 'timesheet-list',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                [
                  'attribute' => 'date',
                  'label' => Yii::t('nc', 'Timesheet'),
                  'value' => function($model){ return $model->title; },
                  'format' => 'ntext',
                  'filter' => DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'date',
                    'type' => DatePicker::TYPE_INPUT,
                    'size' => 'sm'
                  ])
                ],
                'owner.username:ntext:Owner',
                'approvedBy.username:ntext:Approved By',
                'approved_at',
                // 'year',
                // 'week',
                // 'state',
                // 'comment:ntext',
                [ 'attribute' => 'state',
                  'value' => function($model){
                    return $model::state($model['state']);
                  }, 'filter' => Yii::$app->params['timesheet-status']
                ],
                [
                  'class' => 'yii\grid\ActionColumn',
                  'template' => '{view} {approve} {reject} {delete}',
                  'buttons' => [
                    'approve' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-check"></span>', ['approve', 'id' => $model->id, 'state' => 2], [
                          'title' => Yii::t('nc', 'Approve'),
                          'aria-label' => Yii::t('nc', 'Approve'),
                          'data-confirm' => Yii::t('nc', 'Are you sure you want to approve this timesheet?'),
                          'data-method' => 'post',
                          'data-pjax' => '0',
                      ]);
                    },
                    'reject' => function ($url, $model, $key) {
                      return Html::a('<span class="glyphicon glyphicon-ban-circle"></span>', ['approve', 'id' => $model->id, 'state' => 2], [
                          'title' => Yii::t('nc', 'Reject'),
                          'aria-label' => Yii::t('nc', 'Reject'),
                          'data-confirm' => Yii::t('nc', 'Are you sure you want to reject this timesheet?'),
                          'data-method' => 'post',
                          'data-pjax' => '0',
                      ]);
                    }
                  ]],
                  ['class' => 'yii\grid\CheckboxColumn'],
            ],
        ]); ?>
    <?php Pjax::end(); ?>

</div>
