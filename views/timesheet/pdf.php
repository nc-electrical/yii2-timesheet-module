<?php

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ArrayDataProvider;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use nc\timesheet\models\TimeEntry;


$events = ArrayHelper::index($model->entries, 'id', 'date');
$summary = []; $total = 0;
foreach ($model->entries as $e) {
  if (!array_key_exists($key = $e->jobCode->name, $summary)) $summary[$key] = 0;
  $summary[$key] += $e->duration; $total += $e->duration;
}
/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\Timesheet */
?>
<h1><?= $title; ?></h1>

<h3><?= Yii::t('nc', 'Employee'); ?></h3>

<?= DetailView::widget([
  'model' => $model->owner,
  'attributes' => [
    'profile.name',
    'email',
  ]
]); ?>


<?php if ($model->state < 0 && $model->approval_note): ?>
  <h3><?= Yii::t('nc', 'Employer Decission Info'); ?></h3>
    <p class="text-warning"><?= Yii::t('nc', 'The timesheet is rejected by {mgr} at {date} with following reason:', ['mgr' => ArrayHelper::getValue($model->updatedBy, 'username'), 'date' => date('Y-m-d', $model->updated_at)]); ?></p>
    <blockquote><?= $model->approval_note; ?></blockquote>
<?php elseif (($model->state == 1) && $model->approval_note): ?>
  <h3><?= Yii::t('nc', 'Employer Decission Info'); ?></h3>
  <p class="text-success"><?= Yii::t('nc', 'The timesheet is accepted by {mgr} at {date} with following comment:', ['mgr' => ArrayHelper::getValue($model->approvedBy, 'username'), 'date' => $model->approved_at]); ?></p>
  <blockquote><?= $model->approval_note; ?></blockquote>
<?php endif; ?>


<table class="table table-bordered">
    <thead>
      <tr>
      <?php $date = $model->dateFrom;
      $end_date = $model->dateTo;
      $dates = [];
      while ($date <= $end_date){ ?>
        <th><?= date('D d/m', strtotime($date)); ?></th>
      <?php
        $dates[] = $date;
        $date = date('Y-m-d', strtotime("$date + 1 day"));
      }  ?>
      </tr>
    </thead>
    <tbody>
      <tr>
        <?php foreach ($dates as $date): ?>
          <td class="event-panel"><div style="min-height: 200px;">
            <?php foreach (ArrayHelper::getValue($events, $date, []) as $id => $e): ?>
              <?= Html::tag('h5', Yii::t('nc', '{duration} hr<br/>{jobcode}', [
                'duration' => $e['duration'],
                'jobcode' => $e->jobCode->name,
                ]),[
                  'style' => ['color' => $e->jobCode->color]
              ]); ?><hr style="color:<?= $e->jobCode->color; ?>;"/>
            <?php endforeach; ?>
          </div></td>
        <?php endforeach; ?>
      </tr>
    </tbody>
  </table>

  <h3><?= Yii::t('nc', 'Time Entries'); ?></h3>
  <?= GridView::widget([
    'dataProvider' => new ArrayDataProvider(['allModels' => $model->entries]),
    'columns' => [
      'date',
      ['attribute' => 'date', 'value' => function($d){ return date('l', strtotime($d->date)); }, 'format' => 'ntext', 'label' => 'Date of Week'],
      'jobCode.name:ntext:Job Code',
      'description:ntext',
      'duration:integer',
    ]

  ]); ?>
  <p class="text"><?= Yii::t('nc', '<strong>{user}</strong> comment:', ['user' => $model->owner->username]); ?></p>
  <blockquote><?= $model->comment; ?></blockquote>


  <h4 class="text-info"><?= Yii::t('nc', 'Total time spent by Job Code'); ?></h4>
  <ul class="summary">
  <?php foreach($summary as $label => $value): ?>
    <li><strong><?= $label ?>:</strong> <span class="pull-right"><?= $value ?> hr</span></li>
  <?php endforeach; ?>
  </ul>
  <hr>
  <ul class=""><li>
  <strong><?= Yii::t('nc', 'Total') ?>:</strong> <span class="pull-right"><?= $total ?> hr</span></li></ul>
