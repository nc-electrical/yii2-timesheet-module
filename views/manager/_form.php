<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model nc\timesheet\models\ManagerUser */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="manager-user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'manager_id')->widget(Select2::className(), [
            'options' => [
                    'multiple' => false,
                    'placeholder' => '-- Who will be the managers of the below employees? --'
            ],
            'pluginOptions' => [
              'allowClear' => true,
              'minimumInputLength' => 3,
              'ajax' => [
                  'url' => Url::to(['/timesheet/user/lookup']),
                  'dataType' => 'json',
                  'data' => new JsExpression("function(params) { return {q:params.term, o:'$operator'}; }")
              ],
            ]]); ?>

    <?= $form->field($model, 'user_id')->widget(Select2::className(), [
            'options' => [
                    'multiple' => false,
                    'placeholder' => '-- Choose one or more employees here --'
            ],
            'pluginOptions' => [
              'allowClear' => true,
              'minimumInputLength' => 3,
              'ajax' => [
                  'url' => Url::to(['/timesheet/user/lookup']),
                  'dataType' => 'json',
                  'data' => new JsExpression("function(params) { return {q:params.term, o:'$operator'}; }")
              ],
            ]]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('nc', 'Create') : Yii::t('nc', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
