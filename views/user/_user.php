<?php
use kartik\widgets\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

/**
 * @var yii\widgets\ActiveForm      $form
 * @var dektrium\user\models\User   $user
 */
?>

<?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'username')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'password')->passwordInput() ?>
<?= $form->field($user, 'manager_id')->widget(Select2::className(), [
        'options' => [
                'multiple' => false,
                'placeholder' => '-- Who will receive Email when this user submit Leave Request / Timesheet? --'
        ],
        'pluginOptions' => [
          'allowClear' => true,
          'minimumInputLength' => 3,
          'ajax' => [
              'url' => Url::to(['/timesheet/user/lookup']),
              'dataType' => 'json',
              'data' => new JsExpression("function(params) { return {q:params.term, o:'$operator'}; }")
          ],
        ]]); ?>
