<?php

namespace nc\timesheet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use nc\timesheet\models\Timesheet;

/**
 * TimesheetSearch represents the model behind the search form about `nc\timesheet\models\Timesheet`.
 */
class TimesheetSearch extends Timesheet
{

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'approved_by', 'state', 'year', 'week'], 'integer'],
            [['approved_at', 'year', 'comment', 'status', 'approvedByIDs', 'userIDs'], 'safe'],
            [['date', 'dateStart', 'dateEnd'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Timesheet::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'approved_by' => $this->approved_by,
            'approved_at' => $this->approved_at,
            'year' => $this->year,
            'state' => $this->state,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment])
            ->andFilterWhere(['like', 'status', $this->status]);
        if ($this->date)
          $query->andFilterWhere([
            'year' => date('Y', strtotime($this->date)),
            'week' => date('W', strtotime($this->date)),
          ]);
        if ($this->dateStart)
          $query->andFilterWhere(['and',
            ['>=', 'year', date('Y', strtotime($this->dateStart))],
            ['>=', 'week', date('W', strtotime($this->dateStart))],
          ]);
        if ($this->dateEnd)
          $query->andFilterWhere(['and',
            ['<=', 'year', date('Y', strtotime($this->dateEnd))],
            ['<=', 'week', date('W', strtotime($this->dateEnd))],
          ]);
        if (is_array($this->approvedByIDs) && count($this->approvedByIDs))
          $query->andFilterWhere(['in', 'approved_by', $this->approvedByIDs]);
        if (is_array($this->userIDs) && count($this->userIDs))
          $query->andFilterWhere(['in', 'user_id', $this->userIDs]);
        return $dataProvider;
    }
}
