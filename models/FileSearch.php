<?php

namespace nc\timesheet\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use nc\timesheet\models\File;

/**
 * FileSearch represents the model behind the search form about `nc\timesheet\models\File`.
 */
class FileSearch extends File
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['filename', 'type', 'uri', 'alt', 'size', 'description', 'thumbs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => Yii::t('nc', 'File ID'),
            'filename' => Yii::t('nc', 'File name'),
            'type' => Yii::t('nc', 'File Type'),
            'uri' => Yii::t('nc', 'Uri'),
            'alt' => Yii::t('nc', 'Alt'),
            'size' => Yii::t('nc', 'Size'),
            'description' => Yii::t('nc', 'File Description'),
            'thumbs' => Yii::t('nc', 'Thumbs'),
            'created_at' => Yii::t('nc', 'Created At'),
            'updated_at' => Yii::t('nc', 'Updated At'),
            'created_by' => Yii::t('nc', 'Created By'),
            'updated_by' => Yii::t('nc', 'Updated By'),
            'batchUpload' => Yii::t('ecas', 'Files'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = File::find()->orderBy(['updated_at' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'filename', $this->filename])
            ->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'uri', $this->uri])
            ->andFilterWhere(['like', 'alt', $this->alt])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'thumbs', $this->thumbs]);

        return $dataProvider;
    }
}
