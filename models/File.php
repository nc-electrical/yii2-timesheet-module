<?php

namespace nc\timesheet\models;

use yii\helpers\Inflector;
use yii\web\UploadedFile;
use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "{{%files}}".
 *
 * @property integer $id
 * @property string $filename
 * @property string $type
 * @property string $uri
 * @property string $alt
 * @property string $size
 * @property string $description
 * @property string $thumbs
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class File extends \yii\db\ActiveRecord
{
    public $batchUpload;
    public $uploadFile;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%files}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alt', 'description', 'thumbs'], 'string'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['filename', 'uri'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 255],
            [['size'], 'string', 'max' => 20],
            [['batchUpload'], 'file', 'maxFiles' => 99],
            [['uploadFile'], 'file'],
        ];
    }
    function behaviors(){
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::className(),
            'blameable' => \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('nc', 'ID'),
            'filename' => Yii::t('nc', 'Filename'),
            'type' => Yii::t('nc', 'Type'),
            'uri' => Yii::t('nc', 'Uri'),
            'alt' => Yii::t('nc', 'Alt'),
            'size' => Yii::t('nc', 'Size'),
            'description' => Yii::t('nc', 'Description'),
            'thumbs' => Yii::t('nc', 'Thumbs'),
            'created_at' => Yii::t('nc', 'Created At'),
            'updated_at' => Yii::t('nc', 'Updated At'),
            'created_by' => Yii::t('nc', 'Created By'),
            'updated_by' => Yii::t('nc', 'Updated By'),
            'batchUpload' => Yii::t('ecas', 'Files'),
        ];
    }

    function getUpdatedBy(){
      return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    function getCreatedBy(){
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    /**
     * Handle batchUpload
     */
    function saveUpload(){
      $f = $this->uploadFile;
      if ($f instanceof UploadedFile){
        $prefix = date('Y') . '/' . date('m');
        $path = Yii::getAlias("@webroot/uploads/$prefix");
        @mkdir($path, 0775, true);
        $this->filename = Inflector::slug($f->baseName) . "." . $f->extension;
        $this->uri = $prefix . '/' . $this->filename;
        $this->type = $f->type;
        $suffix = 1;
        while (file_exists($this->getPath())){
                $this->uri = $prefix . '/' . Inflector::slug($f->baseName) . "-$suffix." . $f->extension;
                $suffix ++;
        }
        Yii::info("Save file to " . $this->getPath());
        return $f->saveAs($this->getPath());
      } else Yii::error("uploadFile is not UploadedFile instance");
      return true;
    }
    /**
    * Save batch upload
    */
    function saveBatchUpload(){
        $success = [];
        if (count($this->batchUpload)){
            $prefix = date('Y') . '/' . date('m');
            $path = Yii::getAlias("@webroot/uploads/$prefix" );
            @mkdir($path, 0775, true);
            foreach ($this->batchUpload as $f){
                if ($f instanceof UploadedFile){
                        $file = new static();
                        $file->filename = Inflector::slug($f->baseName) . "." . $f->extension;
                        $file->description = ($this->description) .': '. Inflector::humanize($f->baseName);
                        $file->type = $f->type;
                        $suffix = 1;
                        while (file_exists($file->getPath())){
                                $file->uri = $prefix . '/' . Inflector::slug($f->baseName) . "-$suffix." . $f->extension;
                                $suffix ++;
                        }

                        if ($f->saveAs($file->getPath()) && $file->save()){
                                $success[] = $file->uri;
                        }
                }
            }
        }
        return $success;
    }

    function getPath(){
        return Yii::getAlias("@webroot/uploads/" .  $this->uri);
    }

    function getUrl(){
        return Yii::getAlias("@web/uploads/" . $this->uri);
    }

    function beforeDelete(){
      @unlink($this->path);
      return parent::beforeDelete();
    }

}
