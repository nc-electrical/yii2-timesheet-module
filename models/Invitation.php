<?php

namespace nc\timesheet\models;

use Yii;
use dektrium\user\models\User;
/**
 * This is the model class for table "meetinginvitation".
 *
 * @property integer $id
 * @property integer $meeting_id
 * @property integer $user_id
 * @property string $decided_at
 * @property integer $accepted
 */
class Invitation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%meeting_invitation}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['meeting_id', 'user_id', 'accepted'], 'integer'],
            ['note', 'string'],
            [['decided_at', 'note'], 'safe'],
        ];
    }
    function behaviors(){
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::className(),
            'blameable' => \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('nc', 'ID'),
            'meeting_id' => Yii::t('nc', 'Id Meeting'),
            'user_id' => Yii::t('nc', 'Id User'),
            'decided_at' => Yii::t('nc', 'accepted Date'),
            'accepted' => Yii::t('nc', 'accepted'),
        ];
    }

    /** Return user friendly meaning of the status
    * @see params['leave-req-state']
    **/
    static function state($key){
      return ArrayHelper::getValue(Yii::$app->params['invitation-state'], intval($key));
    }

    function getUser(){
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    function getMeeting(){
      return $this->hasOne(Meeting::className(), ['id' => 'meeting_id']);
    }
    function getUpdatedBy(){
      return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    function getCreatedBy(){
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
