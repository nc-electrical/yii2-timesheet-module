<?php

namespace nc\timesheet\models;

use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "timeentry".
 *
 * @property integer $id
 * @property string $timesheet_id
 * @property integer $jobcode_id
 * @property string $date
 * @property double $duration
 * @property string $description
 */
class TimeEntry extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%time_entry}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jobcode_id', 'timesheet_id', 'duration', 'user_id'], 'required'],
            [['jobcode_id', 'timesheet_id', 'user_id'], 'integer'],
            [['date'], 'date', 'format' => 'php:Y-m-d'],
            [['date'], 'compare', 'operator' => '<=', 'compareValue' => date('Y-m-d')],
            [['duration'], 'number', 'max' => 24, 'min' => 0],
            [['duration'], 'validateDuration'],
            [['description'], 'string'],
            // [['timesheet_id'], 'string', 'max' => 10],
        ];
    }

    function behaviors(){
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::className(),
            'blameable' => \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    function validateDuration($attribute, $params){
      $timeSpent = static::find()->where(['and', ['date' => $this->date, 'user_id' => $this->user_id]])->sum('duration');
      $remain = $timeSpent - intval($this->getOldAttribute('duration'));
      if (($remain + intval($this->duration)) > 24) $this->addError($attribute, Yii::t('nc', 'You have entered more than 24 hours on {date} . Please revise your entries. {attribute} must be smaller than {val}', ['val' => 24 - $remain, 'attribute' => $attribute, 'date' => $this->date]));
    }

    function loadDefaultValues($skipIfSet=true){
      $this->duration = 8;
      $this->date = date('Y-m-d');
      $this->created_by = Yii::$app->user->id;
      return parent::loadDefaultValues($skipIfSet);
    }
    /**
    * Create Timesheet if not exists
    */
    function saveTimesheet(){
      $timesheet = Timesheet::loadTimesheet(
        date('Y', strtotime($this->date)),
        date('W', strtotime($this->date))
      );
      $this->setAttribute('timesheet_id', $timesheet->id);
      return true;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('nc', 'ID'),
            'timesheet_id' => Yii::t('nc', 'Id Timesheet'),
            'jobcode_id' => Yii::t('nc', 'Id JobCode'),
            'date' => Yii::t('nc', 'Date'),
            'duration' => Yii::t('nc', 'Duration'),
            'description' => Yii::t('nc', 'Description'),
        ];
    }

    function beforeSave($insert){
      // If duration = 0 mean to delete this timeSheet
      if (!$insert && ($this->duration == 0)) {
        $this->delete();
        return false;
      }
      return parent::beforeSave($insert);
    }

    function getJobCode(){
      return $this->hasOne(JobCode::className(), ['id' => 'jobcode_id']);
    }
    function getTimesheet(){
      return $this->hasOne(Timesheet::className(), ['id' => 'timesheet_id']);
    }

    function getYear(){
      return date('Y', strtotime($this->date));
    }
    function getWeek(){
      return date('W', strtotime($this->date));
    }

    function getTitle(){
      return $this->date;
    }
    function getOwner(){
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    function getUpdatedBy(){
      return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    function getCreatedBy(){
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
