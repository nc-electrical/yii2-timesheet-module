<?php

namespace nc\timesheet\models;
use yii\helpers\ArrayHelper;
use Yii;
use dektrium\user\models\User;

/**
 * This is the model class for table "timesheet".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $approved_by
 * @property string $approved_at
 * @property string $year
 * @property integer $state
 * @property string $comment
 * @property string $status
 */
class Timesheet extends \yii\db\ActiveRecord
{
    // Create Y and W from this date
    public $date;
    // Used in search
    public $dateStart, $dateEnd, $userIDs=[], $approvedByIDs=[];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%timesheet}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'week', 'user_id', 'state'], 'required'],
            [['user_id', 'approved_by', 'state', 'week'], 'integer'],
            [['approved_at', 'year'], 'safe'],
            [['comment', 'approval_note'], 'string'],
            [['status'], 'string', 'max' => 50],
            [['date', 'dateStart', 'dateEnd'], 'date', 'format' => 'php:Y-m-d'],
            [['user_id'], 'unique', 'targetAttribute' => ['year', 'week', 'user_id']],
	          [['approval_note', 'comment'], 'default', 'value' => ''],
            [['userIDs', 'approvedByIDs'], 'safe'],
        ];
    }
    /**
    * Auto-populate the values from date field
    */
    function setDefaultValues($skipIfSet=true){
      $this->user_id = Yii::$app->user->id;
      $this->date = date('Y-m-d');
      return parent::setDefaultValues($skipIfSet);
    }
    function beforeValidate(){
      if ($this->date){
        $this->year = date('Y', strtotime($this->date));
        $this->week = date('W', strtotime($this->date));
      }
      return parent::beforeValidate();
    }
    function getDateFrom(){
      return date('Y-m-d', strtotime("{$this->year}W{$this->week}"));
    }
    function getDateTo(){
      return date('Y-m-d', strtotime($this->getDateFrom() . " + 6 days"));
    }
    /** Return the title of Timesheet **/
    function getTitle(){
      return $this->year . ' Week #' . $this->week;
    }
    static function state($key){
      return ArrayHelper::getValue(Yii::$app->params['timesheet-status'], $key);
    }

    /**
    * Allow load a timesheet with current year and week
    */
    public static function loadTimesheet($year = null, $week = null, $user_id = null){
      if (!$user_id && Yii::$app->user) $user_id = Yii::$app->user->id;
      if (!$year) $year = date('Y');
      if (!$week) $week = date('W');
      $model = static::find()->where($data = [
        'user_id' => $user_id,
        'week' => $week,
        'year' => $year,
      ])->one();
      if (!$model){
        $data['state'] = 0;
        $model = new static;
        $model->setAttributes($data);
        $model->save();
      }
      return $model;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('nc', 'ID'),
            'user_id' => Yii::t('nc', 'Owner'),
            'approved_by' => Yii::t('nc', 'Approved By'),
            'approved_at' => Yii::t('nc', 'Approval Date'),
            'approval_note' => Yii::t('nc', 'Approval Note'),
            'year' => Yii::t('nc', 'Year'),
            'state' => Yii::t('nc', 'State'),
            'comment' => Yii::t('nc', 'Comment'),
            'status' => Yii::t('nc', 'Status'),
            'dateStart' => Yii::t('nc', 'Start Date'),
            'dateEnd' => Yii::t('nc', 'End Date'),
            'approvedByIDs' => Yii::t('nc', 'Approved By Users'),
            'userIDs' => Yii::t('nc', 'Submit by Users'),
        ];
    }

    function behaviors(){
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::className(),
            'blameable' => \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    function getEntries(){
      return $this->hasMany(TimeEntry::className(), ['timesheet_id' => 'id']);
    }
    function beforeDelete(){
      TimeEntry::deleteAll(['timesheet_id' => $this->id]);
      return parent::beforeDelete();
    }

    function getOwner(){
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    function getApprovedBy(){
      return $this->hasOne(User::className(), ['id' => 'approved_by']);
    }
    function getUpdatedBy(){
      return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    function getCreatedBy(){
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    function getApprovedByUsernames(){
      return ArrayHelper::map(User::find()->where(['in', 'id', $this->approvedByIDs])->all(), 'id', 'username');
    }
    function getOwnerUsernames(){
      return ArrayHelper::map(User::find()->where(['in', 'id', $this->userIDs])->all(), 'id', 'username');
    }
}
