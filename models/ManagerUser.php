<?php

namespace nc\timesheet\models;

use Yii;
use yii\db\ActiveRecord;
use dektrium\user\models\User;
/**
 * This is the model class for table "{{%manager_user}}".
 *
 * @property integer $manager_id
 * @property integer $user_id
 *
 * @property User $user
 * @property Reseller $reseller
 */
class ManagerUser extends ActiveRecord
{
    public $managerIDs, $userIDs;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%manager_user}}';
    }
    function scenarios(){
      $rules = parent::scenarios();
      $rules['batch'] = ['managerIDs', 'userIDs'];
      return $rules;
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id', 'user_id'], 'required'],
            [['manager_id', 'user_id'], 'integer'],
        		[['user_id'], 'unique', 'targetAttribute' => ['manager_id', 'user_id']],
            [['managerIDs', 'userIDs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'manager_id' => Yii::t('nc', 'Manager ID'),
            'user_id' => Yii::t('nc', 'User ID'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'manager_id' => Yii::t('nc', 'Reseller ID'),
            'user_id' => Yii::t('nc', 'User ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }
}
