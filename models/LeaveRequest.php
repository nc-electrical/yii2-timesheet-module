<?php

namespace nc\timesheet\models;
use yii\helpers\ArrayHelper;
use Yii;
use dektrium\user\models\User;
/**
 * This is the model class for table "leaverequest".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $approved_by
 * @property integer $state
 * @property string $code
 * @property string $note
 * @property string $approval_note
 * @property string $all_day
 * @property string $start_date
 * @property string $end_date
 * @property integer $number
 * @property string $approved_at
 */
class LeaveRequest extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%leave_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'start_date', 'end_date'], 'required'],
            [['user_id', 'approved_by', 'state', 'number'], 'integer'],
            [['note', 'approval_note'], 'string'],
            [['start_date', 'end_date', 'approved_at'], 'safe'],
            // [['start_date', 'end_date'], 'date', 'format' => 'php:Y-m-d'],
            [['end_date'], 'compare', 'operator' => '>=', 'compareAttribute' => 'start_date'],
            [['code'], 'string', 'max' => 255],
            [['all_day'], 'string', 'max' => 1],
        ];
    }

    function behaviors(){
        return [
            'timestamp' => \yii\behaviors\TimestampBehavior::className(),
            'blameable' => \yii\behaviors\BlameableBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('nc', 'ID'),
            'user_id' => Yii::t('nc', 'Owner'),
            'approved_by' => Yii::t('nc', 'Approved By'),
            'state' => Yii::t('nc', 'State'),
            'code' => Yii::t('nc', 'Code'),
            'note' => Yii::t('nc', 'User Note'),
            'approval_note' => Yii::t('nc', 'Approval User Note'),
            'all_day' => Yii::t('nc', 'All Day'),
            'start_date' => Yii::t('nc', 'Start Date'),
            'end_date' => Yii::t('nc', 'End Date'),
            'number' => Yii::t('nc', 'Number'),
            'approved_at' => Yii::t('nc', 'accepted Date'),
        ];
    }

    function getOwner(){
      return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    function getUpdatedBy(){
      return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    function getCreatedBy(){
      return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    function getApprovedBy(){
      return $this->hasOne(User::className(), ['id' => 'approved_by']);
    }
    /** Return user friendly meaning of the status
    * @see params['leave-req-state']
    **/
    static function state($key){
      return ArrayHelper::getValue(Yii::$app->params['leave-req-state'], intval($key));
    }
}
