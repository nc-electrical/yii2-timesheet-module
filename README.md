# Timesheet Module

This Yii2 module provide a clone of the famous "myworkday" application.

## URL:

__Timesheet__

* User submit timesheet: /timesheet/dashboard/timesheet
* Manager approve timesheet: /timesheet/manager/timesheet
* Administration approve timesheet: /timesheet/admin/timesheet


## Deployment Information

```bash
./yii migrate --migrationPath=@nc/timesheet/migrations
```