<?php

namespace nc\timesheet\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;
use Yii;
use kartik\mpdf\Pdf;
use dektrium\user\models\User;
use nc\timesheet\models\Meeting;
use nc\timesheet\models\File;
use nc\timesheet\models\Invitation;
use nc\timesheet\models\Timesheet;
use nc\timesheet\models\TimeEntry;
use nc\timesheet\models\LeaveRequest;
use nc\timesheet\models\TimeEntrySearch;
/**
 * InvitationController implements the CRUD actions for Invitation model.
 */
class DashboardController extends Controller {

  /**
   * @inheritdoc
   */
  public function behaviors()
  {
      return [
          'verbs' => [
              'class' => VerbFilter::className(),
              'actions' => [
                  'delete, submit, export-pdf, download' => ['POST'],
              ],
          ],
      ];
  }

  /** The main dashboard **/
  function actionIndex(){
    return $this->render('index', [
      'timesheet' => new ActiveDataProvider([
        'query' => Timesheet::find()->where([
          'user_id' => Yii::$app->user->id,
        ])->orderBy(['year' => SORT_DESC, 'week' => SORT_DESC])
      ]),
      'meeting' => new ActiveDataProvider([
        'query' => Meeting::find()->joinWith('invitations')->where([
          'meeting_invitation.user_id' => Yii::$app->user->id,
        ])->orderBy(['start_date' => SORT_DESC])
      ]),
      'leaveRequest' => new ActiveDataProvider([
        'query' => LeaveRequest::find()->where([
          'user_id' => Yii::$app->user->id,
        ])->orderBy(['start_date' => SORT_DESC])
      ]),
      'files' => new ActiveDataProvider([
        'query' => File::find()->where([
          'created_by' => Yii::$app->user->id,
        ])->orderBy(['updated_at' => SORT_DESC])
      ]),
    ]);
  }
  /**
  * Create a leave request
  */
  function actionMeeting($id = null){
    if (!$id){
      $model = new Meeting();
      $model->setAttributes([
        'user_id' => Yii::$app->user->id,
      ]);
    } else {
      $model = Meeting::findOne($id);
      if ($model->user_id != Yii::$app->user->id) {
        $invitation = Invitation::findOne(['meeting_id' => $id, 'user_id' => Yii::$app->user->id]);
        if ($invitation->load(Yii::$app->request->post()) && $invitation->validate()) {
          $invitation->decided_at = date('Y-m-d H:i:s');
          $invitation->save();
          Yii::$app->session->addFlash('success', 'Successfully save invitation');
        }
        return $this->render('invitation', ['model' => $model, 'invitation' => $invitation]);
      }
    }
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        $model->saveUser();
        return $this->redirect(['index']);
    } else {
        $model->loadUser();
        return $this->render('meeting', [
            'model' => $model,
        ]);
    }
  }
  /**
  * Create a leave request
  */
  function actionLeaveRequest($id = null){
    if (!$id || (!($model = LeaveRequest::findOne($id)))){
      $model = new LeaveRequest();
    }
    $model->setAttributes([
      'user_id' => Yii::$app->user->id,
    ]);
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        return $this->redirect(['index']);
    } else {
        return $this->render('leave-request', [
            'model' => $model,
        ]);
    }
  }

  /**
  * Allow every one to fill in the Timesheet of their own
  */
  function actionTimesheet($year=null, $week=null){
    $model = Timesheet::loadTimesheet($year, $week);
    if ($model->load(Yii::$app->request->post()) && $model->save()) {
        Yii::$app->session->addFlash('success', 'Successfully save Timesheet for Approval');
        return $this->redirect(['index']);
    }
    return $this->render('timesheet', [
      'model' => $model,
    ]);
  }
  /**
  * Populate data for Timesheet
  */
  public function actionTimeEntries($start=NULL,$end=NULL,$_=NULL){
    // if (!Yii::$app->request->isAjax) throw new NotFoundHttpException('The requested page does not exist.');
    Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    // $timesheet = Timesheet::loadTimesheet();
    $searchModel = new TimeEntrySearch();
    $searchModel->dateFrom = $start;
    $searchModel->dateTo = $end;
    $searchModel->user_id = Yii::$app->user->id;
    $dataProvider = $searchModel->search([]);
    $dataProvider->pagination = false;
    $events = ArrayHelper::getColumn($dataProvider->getModels(), 'event');
    return $events;
  }

  /**
  * Save Time Entry request from Ajax form
  */
  public function actionSaveTimeEntry(){
    $post = Yii::$app->request->post();
    if ($id = ArrayHelper::getValue($post, 'TimeEntry.id')){
      $model = TimeEntry::findOne($id);
    } else {
      $model = new TimeEntry();
    }
    $model->user_id = Yii::$app->user->id;
    if ($model->load($post) && $model->saveTimesheet() && $model->save()) {
        Yii::$app->session->addFlash('success', 'Successfully add Entry to timesheet');
    } else {
        Yii::$app->session->addFlash('warning', 'Failed to save Timesheet');
        foreach ($model->getFirstErrors() as $field => $message){
          Yii::$app->session->addFlash('danger', $message);
        }
    }
    return $this->redirect(['timesheet', 'year' => $model->year, 'week' => $model->week]);
  }

  /**
  * Feed data to Select2 to lookup user
  */
  public function actionLookup($q=null, $id=null, $o=null){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $out = ['results' => ['id' => '', 'text' => '']];
      if (!is_null($q)){
        $models = User::find()->where(['like', 'username', $q])->all();
        $out['results'] = \yii\helpers\ArrayHelper::getColumn($models, function($element){
          return ['id' => $element['id'], 'text' => $element['username']];
        });
      } else if ($id){
        $out['results'] = ['id' => $id, 'text' => $this->findModel($id)->title];
      }
      return $out;
    }


    public function actionExportPdf() {
        // get your HTML raw content without any layouts or scripts
        // $content = $this->renderPartial('_reportView');
        $content = Yii::$app->request->post('content');

        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            // set to use core fonts only
            'mode' => Pdf::MODE_CORE,
            // A4 paper format
            'format' => Pdf::FORMAT_A4,
            // portrait orientation
            'orientation' => Pdf::ORIENT_PORTRAIT,
            // stream to browser inline
            'destination' => Pdf::DEST_BROWSER,
            // your html content input
            'content' => $content,
            // format content from your own css file if needed or use the
            // enhanced bootstrap css built by Krajee for mPDF formatting
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            // any css to be embedded if required
            'cssInline' => '.kv-heading-1{font-size:18px}',
             // set mPDF properties on the fly
            // 'options' => ['title' => 'Krajee Report Title'],
             // call mPDF methods on the fly
            'methods' => [
                // 'SetHeader'=>['Krajee Report Header'],
                'SetFooter'=>['{PAGENO}'],
            ]
        ]);

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    function actionUploadFile(){
      $model = new File();
      if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          $model->uploadFile = UploadedFile::getInstance($model, 'uploadFile');
          if ($model->saveUpload() && $model->save()){
            Yii::$app->session->addFlash('success', 'Successfully save file.');
            return $this->redirect(['index']);
          } else {
            Yii::$app->session->addFlash('error', 'Cannot save upload file due to ' . json_encode($model->getFirstErrors()));
          }
      }
      return $this->render('upload-file', [
          'model' => $model,
      ]);
    }

    function actionBatchUpload(){
      $model = new File();
      if ($model->load(Yii::$app->request->post()) && $model->validate()) {
          $model->batchUpload = UploadedFile::getInstances($model, 'batchUpload');
          if ($cnt = $model->saveBatchUpload()){
            Yii::$app->session->addFlash('success', "Successfully create $cnt file");
          }
          return $this->redirect(['index']);
      } else {
        return $this->render('batch-upload', [
          'model' => $model
        ]);
      }
    }
}
