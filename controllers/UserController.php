<?php
namespace nc\timesheet\controllers;

use yii\web\Controller;
use yii\helpers\ArrayHelper;
use Yii;
use dektrium\user\models\User;
/**
 * InvitationController implements the CRUD actions for Invitation model.
 */
class UserController extends Controller {

  /**
  * Feed data to Select2
  */
  public function actionLookup($q=null, $id=null, $o=null){
      \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $out = ['results' => ['id' => '', 'text' => '']];
      if (!is_null($q)){
        $models = User::find()->where(['like', 'username', $q])->all();
        $out['results'] = ArrayHelper::getColumn($models, function($element){
          return ['id' => $element['id'], 'text' => $element['username']];
        });
      } else if ($id){
        $out['results'] = ['id' => $id, 'text' => ArrayHelper::getValue(User::findOne($id), 'title')];
      }
      return $out;
    }
}
