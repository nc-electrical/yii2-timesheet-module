<?php

namespace nc\timesheet\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use kartik\mpdf\Pdf;
use Yii;
use nc\timesheet\models\Timesheet;
use nc\timesheet\models\TimesheetSearch;
use nc\timesheet\models\ManagerUser;

/**
 * TimesheetController implements the CRUD actions for Timesheet model.
 */
class TimesheetController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete, approve, batch-approval' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Timesheet models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimesheetSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // Extra checking if current user can see only see his own employees ...
        if (! Yii::$app->user->can('/timesheet/*')){
          // Must be a manager?
          $ownerIDs = ArrayHelper::getColumn(ManagerUser::findAll(['manager_id' => Yii::$app->user->id]), 'user_id');
          $dataProvider->query->andFilterWhere(['in', 'user_id', $ownerIDs]);
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Timesheet model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id = null)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Successfully save Timesheet');
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionExportPdf($id = null) {
      $model = $this->findModel($id);
        $title = Yii::t('nc', 'Timesheet {from} to {to}', ['from' => Yii::$app->formatter->asDate($model->dateFrom, 'short'), 'to' => Yii::$app->formatter->asDate($model->dateTo, 'short')]);
        $content = $this->renderPartial('pdf', ['model' => $model, 'title' => $title]);
        // return $content;
        // setup kartik\mpdf\Pdf component
        $pdf = new Pdf([
            'mode' => Pdf::MODE_CORE,
            'format' => Pdf::FORMAT_A4,
            'orientation' => Pdf::ORIENT_PORTRAIT,
            'destination' => Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
             // call mPDF methods on the fly
            'methods' => [
                'SetHeader'=>[$title],
                'SetFooter'=>[Yii::$app->name, '{PAGENO}'],
            ],
            'options' => ['title' => $title],
        ]);
        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Creates a new Timesheet model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Timesheet();
    //
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing Timesheet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    // public function actionUpdate($id)
    // {
    //     $model = $this->findModel($id);
    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => $model->id]);
    //     } else {
    //         $model->date = $model->dateFrom;
    //         return $this->render('update', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing Timesheet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionApprove($id, $state = 2)
    {
        $model = $this->findModel($id);
        $model->updateAttributes([
          'approved_by' => Yii::$app->user->id,
          'approved_at' => date('Y-m-d'),
          'state' => $state,
        ]);
        // TODO: Sending email to customer
        return $this->redirect(['index']);
    }

    /**
     * Updates an existing Timesheet model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBatchApproval($state)
    {
        $ids = Yii::$app->request->post('row_id');
        $timesheet = Timesheet::find()->where(['in', 'id', $ids])->all();
        foreach ($timesheet as $ts){
          $ts->updateAttributes([
            'approved_by' => Yii::$app->user->id,
            'approved_at' => date('Y-m-d'),
            'state' => $state,
          ]);
        }
        // TODO: Sending email to customer
        return $this->redirect(['index']);
    }


    /**
     * Deletes an existing Timesheet model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
     public function actionDelete($id)
     {
	 $model = $this->findModel($id);
	 if ($model->delete()) Yii::$app->session->addFlash('success', Yii::t('nc', 'Successfully delete timesheet {name}', ['name' => $model->title]));
         return $this->redirect(['index']);
     }

    /**
     * Finds the Timesheet model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Timesheet the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
      if (! Yii::$app->user->can('/timesheet/*')){
        // Must be a manager?
        $ownerIDs = ArrayHelper::getColumn(ManagerUser::findAll(['manager_id' => Yii::$app->user->id]), 'user_id');
        if (!in_array($id, $ownerIDs)) throw new NotFoundHttpException('The requested page does not exist.');
      }
        if (($model = Timesheet::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
