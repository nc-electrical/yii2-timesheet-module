<?php

namespace nc\timesheet\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use Yii;
use nc\timesheet\models\TimeEntry;
use nc\timesheet\models\TimeEntrySearch;

/**
 * TimeEntryController implements the CRUD actions for TimeEntry model.
 */
class TimeEntryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TimeEntry models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimeEntrySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TimeEntry model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TimeEntry model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TimeEntry();

        if ($model->load(Yii::$app->request->post()) && $model->saveTimesheet() && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $model->loadDefaultValues();
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TimeEntry model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->saveTimesheet() && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing TimeEntry model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TimeEntry model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimeEntry the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TimeEntry::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
    * Feed data to jQuery Full Calendar
    */
    public function actionJson($start=NULL,$end=NULL,$_=NULL){
      // if (!Yii::$app->request->isAjax) throw new NotFoundHttpException('The requested page does not exist.');
      Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
      $searchModel = new TimeEntrySearch();
      $searchModel->dateFrom = $start;
      $searchModel->dateTo = $end;
      $dataProvider = $searchModel->search([]);
      $dataProvider->pagination = false;
      $events = ArrayHelper::getColumn($dataProvider->getModels(), 'event');
      return $events;
    }
    /**
    * Save data
    */
    public function actionSave(){
      $post = Yii::$app->request->post();
      if ($id = ArrayHelper::getValue($post, 'TimeEntry.id')){
        $model = $this->findModel($id);
      } else {
        $model = new TimeEntry();
      }
      if ($model->load($post) && $model->saveTimesheet() && $model->save()) {
          return $this->redirect(Yii::$app->request->referrer);
      } else {
          $model->loadDefaultValues();
          return $this->render('create', [
              'model' => $model,
          ]);
      }
    }
}
