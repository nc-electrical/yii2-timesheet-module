<?php

namespace nc\timesheet\controllers;

use Yii;
use nc\timesheet\models\ManagerUser;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ManagerController implements the CRUD actions for ManagerUser model.
 */
class ManagerController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ManagerUser models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ManagerUser::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
    * Allow create multiple user / manager relationship at once
    */
    function actionBatch(){
      $model = new ManagerUser();
       $model->scenario = 'batch';
       if ($model->load(Yii::$app->request->post()) && $model->validate()) {
         foreach ($model->managerIDs as $m){
           foreach ($model->userIDs as $u){
             $item = new ManagerUser();
             $item->user_id = $u; $item->manager_id = $m;
             if ($item->save()){
                     Yii::$app->session->addFlash('success', Yii::t('nc', 'Successfully create ManagerUser {name}', ['name' => $name]));
             } else {
                     Yii::$app->session->addFlash('warning', Yii::t('nc', 'Unable to create ManagerUser {name}', ['name' => $name]));
             }
           }
         }
         return $this->redirect(['index']);
       }
       return $this->render('batch', ['model' => $model]);

    }

    /**
     * Displays a single ManagerUser model.
     * @param integer $manager_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionView($manager_id, $user_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($manager_id, $user_id),
        ]);
    }

    /**
     * Creates a new ManagerUser model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ManagerUser();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'manager_id' => $model->manager_id, 'user_id' => $model->user_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ManagerUser model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $manager_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionUpdate($manager_id, $user_id)
    {
        $model = $this->findModel($manager_id, $user_id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'manager_id' => $model->manager_id, 'user_id' => $model->user_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ManagerUser model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $manager_id
     * @param integer $user_id
     * @return mixed
     */
    public function actionDelete($manager_id, $user_id)
    {
        $this->findModel($manager_id, $user_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ManagerUser model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $manager_id
     * @param integer $user_id
     * @return ManagerUser the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($manager_id, $user_id)
    {
        if (($model = ManagerUser::findOne(['manager_id' => $manager_id, 'user_id' => $user_id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
