<?php
/*
 * ################################################
 * # 1 - Dependency Injection for common widgets #
 * ################################################
 */
/**
* Web Application Customization for our application
*/
$commonLocal = [
	'id' => 'nc-electrical',
	'name' => 'NC Electrical',
	'runtimePath' => '/tmp',
/*##########################
 # 2 - Custom components #
##########################*/
		'components' => [
			'db' => [
					'class' => 'yii\db\Connection',
					'dsn' => 'mysql:host=db;dbname=nc',
					'username' => 'nc',
					'password' => 'nc123456',
					'charset' => 'utf8',
					'schemaCache' => true,
			],
			'i18n' => [
					'translations' => [
							'*' => [
									'class' => 'yii\i18n\PhpMessageSource',
									'basePath' => '@app/messages'
							]
					]
			],
		],
/*###############
 # 3 - Modules #
 ###############*/
		 'modules' => [
			 'timesheet' => 'nc\timesheet\TimesheetModule',
		 ],
];

/*#########################################
 # 4 - Developement Specific Config       #
 ##########################################*/
if (YII_DEBUG) {

}

return $commonLocal;
?>
