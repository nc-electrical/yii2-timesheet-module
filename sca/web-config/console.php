<?php
/**
* Web Application Customization for our application
*/
$consoleLocal = [
/*##########################
 # 2 - Custom components #
##########################*/
	'components' => [
		'urlManager' => [ 'hostInfo' => 'http://gateway.foxtech.com.vn'],
	],
];

/*#########################################
 # 4 - Developement Specific Config       #
 ##########################################*/
if (YII_DEBUG) {

}

return $consoleLocal;
?>
