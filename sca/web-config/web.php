<?php
/**
* Global Customization for our application
*/
$webLocal = [
/*##########################
 # 2 - Custom components #
##########################*/
		'bootstrap' =>  ['languagepicker'],
		'defaultRoute' => 'timesheet/site/index',
		'components' => [
			'urlManager' => [
			    'enablePrettyUrl' => true,
			    'showScriptName' => false,
			    'rules' => [
				['class' => 'yii\rest\UrlRule', 'controller' => '/gw/web-service'],
				'gateway/web/sdp/rest/create' => 'gw/web-service/create',
				'gateway/web/sdp/soap/wsdl' => 'gw/web-service/wsdl',
				'gateway/web/sdp/soap/sms' => 'gw/web-service/sms',
			    ],
			],
			'languagepicker' => [
				'class' => 'lajax\languagepicker\Component',
				'languages' => ['en', 'vi'],         // List of available languages (icons only)
				'cookieName' => 'lang',                         // Name of the cookie.
				'expireDays' => 64,                                 // The expiration time of the cookie is 64 days.
			],
		],
		'as access' => [
				'class' => 'mdm\admin\components\AccessControl',
				'allowActions' => [
						'site/*',
						'user/security/*', 'user/recovery/*', 'user/register/*', 'user/settings/*', 'user/registration/*',
				]
		]
];

/*#########################################
 # 4 - Developement Specific Config       #
 ##########################################*/
if (YII_DEBUG) {

}

return $webLocal;
?>
