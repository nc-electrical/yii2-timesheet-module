<?php

return [
	/* System Email Parameters **/
		'adminEmail' => 'support@foxtech.com.vn',
		'noreplyEmail' => 'no-reply@foxtech.com.vn',
		'signatureTitle' => 'Foxtech Gateway Support Team',
		'signatureOrganization' => 'Foxtech LTD,',
		'supportEmail' => 'support@foxtech.com.vn',
		'signatureAddress' => '149 Lo Duc, Hai Ba Trung district - Ha Noi city - Viet Nam',
	/** End of System Email Parameters **/
	'timesheet-status' => [
          0 => 'Not yet verified',
          1 => 'Submited',
          2 => 'Approved',
          -1 => 'Rejected',
          -2 => 'Returned for Correction',
  ],
  /** Transition state for Leave Request **/
  'leave-req-state' => [
          0 => 'Not yet verified',
          1 => 'Submited',
          2 => 'Approved',
          -1 => 'Rejected',
  ],

];
