<?php

use yii\db\Migration;

class m161030_143915_rbac extends Migration
{
    public $roles, $perms;
    function init(){
      $this->perms = [
        '/timesheet/*',
        '/timesheet/dashboard/*',
        '/timesheet/file/*',
        '/timesheet/job-code/*',
        '/timesheet/leave-request/*',
        '/timesheet/meeting/*',
        '/timesheet/timesheet/*',
        '/user/admin/*',
        '/auth/*',
      ];
      $this->roles = [
        'Admin', 'Manager', 'Employee'
      ];
    }
    public function up()
    {
      // Create all roles
      foreach ($this->roles as $roleName){
        if (! Yii::$app->authManager->getRole($roleName)){
          echo("Create role $roleName\n");
          $item = Yii::$app->authManager->createRole($roleName);
          Yii::$app->authManager->add($item);
        }
      }
      // Create all permission
      foreach ($this->perms as $roleName){
        if (! Yii::$app->authManager->getPermission($roleName)){
          echo("Create permission $roleName\n");
          $item = Yii::$app->authManager->createPermission($roleName);
          Yii::$app->authManager->add($item);
        }
      }
      Yii::$app->authManager->createPermission('/user/admin/*');
      Yii::$app->authManager->createPermission('/auth/*');
      // Create Assignment Structure
      // Add permmission for Employee
      $parent = Yii::$app->authManager->getRole('Employee');
      foreach (['/timesheet/dashboard/*', '/timesheet/file/*'] as $perm){
        if ($child = Yii::$app->authManager->getPermission($perm)){
          Yii::$app->authManager->addChild($parent, $child);
        }
      }
      // Add permmission for Manager
      $parent = Yii::$app->authManager->getRole('Manager');
      $child = Yii::$app->authManager->getRole('Employee');
      Yii::$app->authManager->addChild($parent, $child);
      foreach (['/timesheet/job-code/*', '/timesheet/leave-request/*', '/timesheet/meeting/*', '/timesheet/timesheet/*'] as $perm){
        if ($child = Yii::$app->authManager->getPermission($perm)){
          Yii::$app->authManager->addChild($parent, $child);
        }
      }
      // Add permission for Admin
      $parent = Yii::$app->authManager->getRole('Admin');
      $child = Yii::$app->authManager->getRole('Manager');
      Yii::$app->authManager->addChild($parent, $child);
      foreach (['/timesheet/*', '/user/admin/*'] as $perm){
        if ($child = Yii::$app->authManager->getPermission($perm)){
          Yii::$app->authManager->addChild($parent, $child);
        }
      }
    }

    public function down()
    {
      foreach ($this->roles as $roleName){
        if ($item = Yii::$app->authManager->getRole($roleName)){
          Yii::$app->authManager->remove($item);
        }
      }
      // Create all permission
      foreach ($this->perms as $roleName){
        if ($item = Yii::$app->authManager->getPermission($roleName)){
          Yii::$app->authManager->remove($item);
        }
      }
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
