<?php

use yii\db\Migration;

class m160926_015651_file extends Migration
{
  public $tableOption;
  public function init()
  {
      if ($this->db->driverName === 'mysql') {
          $this->tableOption = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
      }
  }
    public function safeUp()
    {
      $this->createTable('{{%files}}', [
            'id' => 'pk',
            'filename' => 'string',
            'type' => 'string(40)',
            'uri' => 'string',
            'alt' => 'text',
            'size' => 'string(20)',
            'description' => 'text',
            'thumbs' => 'text',
            'created_at' => 'integer',
            'updated_at' => 'integer',
            'created_by' => 'integer',
            'updated_by' => 'integer',
        ], $this->tableOption);
    }

    public function safeDown()
    {
      $this->dropTable('{{%files}}', $this->tableOption);
    }
}
