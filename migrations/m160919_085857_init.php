<?php

use yii\db\Migration;
use yii\db\Schema;

class m160919_085857_init extends Migration
{
    /** Table option applied to all tables **/
  public $tableOption;
  public function init()
  {
      if ($this->db->driverName === 'mysql') {
          $this->tableOption = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
      }
  }

  /** Upgrade database **/
  public function safeUp()
  {
      $this->addColumn('{{%user}}', 'manager_id', 'int');
      /*** Table structure for `job_code` ***/
      $this->createTable('{{%job_code}}', [
                      'id' => Schema::TYPE_PK,
                      'name' => 'string',
                      'description' => 'text',
                      'color' => 'string(10)',
                      // Behavior columns
                      'created_at' => 'int',
                      'updated_at' => 'int',
                      'created_by' => 'int',
                      'updated_by' => 'int',
      ], $this->tableOption);

      /*** Table structure for `leave_request` ***/
      $this->createTable('{{%leave_request}}', [
                      'id' => Schema::TYPE_PK,
                      'user_id' => 'int',
                      'code' => 'string',
                      'note' => 'text',
                      'all_day' => 'boolean',
                      'start_date' => 'date',
                      'end_date' => 'date',
                      'number' => 'smallint',
                      'state' => 'smallint(3)',
                      'approved_by' => 'int',
                      'approved_at' => 'date',
                      'approval_note' => 'text',
                      // Behavior columns
                      'created_at' => 'int',
                      'updated_at' => 'int',
                      'created_by' => 'int',
                      'updated_by' => 'int',
      ], $this->tableOption);
      $this->addForeignKey('fk_leave_req_user', '{{%leave_request}}', 'user_id', '{{%user}}', 'id');

      /*** Table structure for `invitation` ***/
      $this->createTable('{{%meeting_invitation}}', [
                      'id' => Schema::TYPE_PK,
                      'meeting_id' => 'int',
                      'user_id' => 'int',
                      'decided_at' => 'datetime',
                      'accepted' => 'boolean',
                      // Behavior columns
                      'created_at' => 'int',
                      'updated_at' => 'int',
                      'created_by' => 'int',
                      'updated_by' => 'int',
      ], $this->tableOption);

      /*** Table structure for `meeting` ***/
      $this->createTable('{{%meeting}}', [
                      'id' => Schema::TYPE_PK,
                      'user_id' => 'int',
                      'name' => 'string',
                      'note' => 'text',
                      'all_day' => 'boolean',
                      'start_date' => 'datetime',
                      'end_date' => 'datetime',
                      // Behavior columns
                      'created_at' => 'int',
                      'updated_at' => 'int',
                      'created_by' => 'int',
                      'updated_by' => 'int',
      ], $this->tableOption);
      $this->addForeignKey('fk_meeting_invitation', '{{%meeting_invitation}}', 'meeting_id', '{{%meeting}}', 'id');
      $this->addForeignKey('fk_meeting_owner', '{{%meeting}}', 'user_id', '{{%user}}', 'id');
      /*** Table structure for `time_entry` ***/
      $this->createTable('{{%time_entry}}', [
                      'id' => Schema::TYPE_PK,
                      'timesheet_id' => 'int',
                      'jobcode_id' => 'int',
                      'date' => 'date',
                      'duration' => 'float',
                      'description' => 'text',
                      'user_id' => 'int',
                      // Behavior columns
                      'created_at' => 'int',
                      'updated_at' => 'int',
                      'created_by' => 'int',
                      'updated_by' => 'int',
      ], $this->tableOption);
      $this->addForeignKey('fk_entry_owner', '{{%time_entry}}', 'user_id', '{{%user}}', 'id');

      /*** Table structure for `time_entry` ***/
      $this->createTable('{{%timesheet}}', [
                      'id' => Schema::TYPE_PK,
                      'user_id' => 'int',
                      'approved_by' => 'int',
                      'approved_at' => 'date',
                      'approval_note' => 'text',
                      'year' => 'int(4)',
                      'week' => 'smallint',
                      'state' => 'smallint(2)',
                      'comment' => 'text',
                      'status' => 'string(50)',
                      // Behavior columns
                      'created_at' => 'int',
                      'updated_at' => 'int',
                      'created_by' => 'int',
                      'updated_by' => 'int',
      ], $this->tableOption);
      $this->addForeignKey('fk_sheet_owner', '{{%timesheet}}', 'user_id', '{{%user}}', 'id');
      $this->addForeignKey('fk_sheet_approver', '{{%timesheet}}', 'approved_by', '{{%user}}', 'id');

      $this->createTable('{{%manager_user}}', [
    		'manager_id' => 'int',
    		'user_id' => 'int',
      ], $this->tableOption);
      $this->addPrimaryKey('pk_manager_user', '{{%manager_user}}', ['manager_id', 'user_id']);
      $this->addForeignKey('fk_manager_user_uid', '{{%manager_user}}', 'user_id', '{{%user}}', 'id');
      $this->addForeignKey('fk_manager_user_mid', '{{%manager_user}}', 'manager_id', '{{%user}}', 'id');
  }

  public function safeDown()
  {
    $this->dropTable('{{%manager_user}}');
    $this->dropTable('{{%time_entry}}');
    $this->dropTable('{{%timesheet}}');
    $this->dropTable('{{%meeting_invitation}}');
    $this->dropTable('{{%meeting}}');
    $this->dropTable('{{%leave_request}}');
    $this->dropTable('{{%job_code}}');
    $this->dropColumn('{{%user}}', 'manager_id');
  }
}
